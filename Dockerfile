# syntax=docker/dockerfile:1
FROM jupyter/datascience-notebook
COPY requirements.txt ./
# COPY jupyter/ ./

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
    

