import numpy as np
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt
import utils as su
import matplotlib.patches as mpatches

def get_color(i):
    colors = ["blue", "gray", "red", "green", "orange", "yellow", "black"]
    return colors[i%7]

def matplot_valve(axes, valve):
    maxVal, minVal = -10000000, 1000000
    patches = []
    for i in range(len(valve.submeshes)):
        submesh = valve.submeshes[i]
        submesh.mesh.unmerge_vertices()
        polys = submesh.mesh.vertices.copy().reshape(-1,3,3)
        axes.add_collection3d(mplot3d.art3d.Poly3DCollection(polys, alpha=0.5,\
                                                             facecolors=get_color(i)))
    
        patches.append(mpatches.Patch(color=get_color(i), label="submesh {0}".format(i)))
        maxVal = max(maxVal, submesh.mesh.vertices.max())
        minVal = min(minVal, submesh.mesh.vertices.min())
    
        submesh.mesh.merge_vertices()
    
    scale = [minVal,maxVal]
    axes.auto_scale_xyz(scale, scale, scale)
    axes.legend(handles=patches)

def matplot_mesh(axes, submesh, plotInt=False, addZAxis=True, c="blue"):
    # Load the STL files and add the vectors to the plot
    submesh.mesh.unmerge_vertices()
    polys = submesh.mesh.vertices.copy().reshape(-1,3,3)
    axes.add_collection3d(mplot3d.art3d.Poly3DCollection(polys, alpha=0.5, facecolors=c))
    
    # Auto scale to the mesh size,
    scale = submesh.mesh.vertices
    
    if(addZAxis):
        axes.plot([0,0],[0,0],[submesh.mesh.vertices[:,2].min(),submesh.mesh.vertices[:,2].max()])
    
    
    centroids = np.array([su.get_face_centroid(submesh.mesh,i) for i in range(len(submesh.mesh.face_normals))])
    
    if(plotInt):
        X, Y, Z = submesh.intPoints[:,0], submesh.intPoints[:,1], submesh.intPoints[:,2]
        U, V, W = submesh.intNormals[:,0]*1.5, submesh.intNormals[:,1]*1.5, submesh.intNormals[:,2]*1.5
        axes.plot(X,Y,Z,c='r')
    else: 
        X, Y, Z = centroids[:,0], centroids[:,1], centroids[:,2]
        U, V, W = submesh.mesh.face_normals[:,0], submesh.mesh.face_normals[:,1], submesh.mesh.face_normals[:,2]
    
    axes.scatter(X,Y,Z,c='r')
    axes.quiver(X, Y, Z, U, V, W)
    
    axes.auto_scale_xyz(scale[:,0], scale[:,0], scale[:,0])
    submesh.mesh.merge_vertices()

    
def plot_convex_hull(axes, submesh):
    hullPoints = submesh.p_points[submesh.p_hull.vertices]
    
    axes.scatter(submesh.p_points[:,0], submesh.p_points[:,1], alpha=1, s=2)
    axes.plot(np.concatenate([hullPoints[:,0],[hullPoints[0,0]]]),\
              np.concatenate([hullPoints[:,1],[hullPoints[0,1]]]), 'r', lw=2)
    
def plot_points_hull(axes, submesh, grid):
    mask = su.points_in_hull(grid, submesh.p_hull)
    hullPoints = submesh.p_points[submesh.p_hull.vertices]

    axes.plot(np.concatenate([hullPoints[:,0],[hullPoints[0,0]]]),\
              np.concatenate([hullPoints[:,1],[hullPoints[0,1]]]), 'r', lw=2)
    axes.plot(grid[:,0], grid[:,1])
    axes.scatter(grid[mask,0], grid[mask,1], c='g')