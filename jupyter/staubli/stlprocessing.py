import numpy as np
from mpl_toolkits import mplot3d
from scipy.spatial.transform import Rotation as R
from matplotlib import pyplot as plt
from scipy.spatial import ConvexHull, convex_hull_plot_2d
from scipy.spatial import Delaunay
import sklearn.preprocessing
import trimesh
import utils as su
from enum import Enum
import logging

class StlMesh:
    def __init__(self, file, new=False):
        self.mesh = trimesh.load(file)

        self.remove_unconnected_faces()
        surfaces = self.get_all_surfaces(np.pi/8.)
        logging.info("Found {0} surfaces while processing the mesh".format(len(surfaces)))
        
        subtemp = []
        for i, surface in enumerate(surfaces):
            subtemp.append(StlSubmesh(self.mesh,surface,i))
        
        self.submeshes = np.array(subtemp)
        
        
    # preprocessing functions
    
    def build_face_adjacency_graph(self):
        graph = -np.ones((len(self.mesh.faces),3))
        for i in range(len(self.mesh.faces)):
            neigh = np.append(self.mesh.face_adjacency[self.mesh.face_adjacency[:,1] == i][:,0],\
                              self.mesh.face_adjacency[self.mesh.face_adjacency[:,0] == i][:,1])
            if len(neigh) < 3:
                neigh = np.append(neigh, -np.ones(3-len(neigh)))
            graph[i] = neigh
        return graph.astype(int)
    
    def build_face_adjacency_graph2(self):
        graph = -np.ones((len(self.mesh.faces),3))
        for i in range(len(self.mesh.faces)):
            connect = []
            for e in range(3):
                for j in range(len(self.mesh.faces)):
                    if(j == i):
                        continue
                    if(self.mesh.faces[i][e] in self.mesh.faces[j] and self.mesh.faces[i][(e+1)%3] in self.mesh.faces[j]):
                        connect.append(j)
                        break
                if(len(connect) > 2):
                    break
                    
            graph[i] =[connect[k] if k<len(connect) else -1 for k in range(3)]
        return graph.astype(int)
    
    def remove_unconnected_faces(self):
        mask = np.ones(len(self.mesh.faces),dtype=bool)
        graph = self.build_face_adjacency_graph()
        for i,l in enumerate(graph):
            if(l[0] == l[1] and l[1] == l[2]):
                mask[i] = False
                
        self.mesh.update_faces(mask)
        
    # subdividing into different surfaces
    
    def get_surface_connection(self, faceId, graph, maxAngle=np.pi/8.):
        curNorm = self.mesh.face_normals[faceId]
        visited = np.zeros(len(self.mesh.faces), dtype=bool)
        queue = [faceId]
        surface = [faceId]
        visited[faceId] = True
        
        while queue:
            i = queue.pop(0)
            curNorm = self.mesh.face_normals[i]
    
            for neighbour in graph[i]:
                if not visited[neighbour] and neighbour>-1:
                    if np.arccos(min(curNorm.dot(self.mesh.face_normals[neighbour]),1)) < maxAngle:
                        queue.append(neighbour)
                        surface.append(neighbour)
                        visited[neighbour] = True
                    
        return surface
    
    def change_face_colors(self, ids, color):
        for i in ids:
            self.mesh.visual.face_colors[i] = color
            
    def get_all_surfaces(self, maxAngle=np.pi/8.):
        graph = self.build_face_adjacency_graph()
        surfaces = [self.get_surface_connection(0,graph,maxAngle)]
        checked = surfaces[0]
        avgArea = self.mesh.area/len(self.mesh.faces)
        
        for i in range(len(self.mesh.faces)):
            if i not in checked:
                surface = self.get_surface_connection(i,graph,maxAngle)
                if(len(surface) < 10):
                    area = 0
                    for elem in surface:
                        area = self.mesh.area_faces[elem]
                    if(area/len(surface) < avgArea*0.01):
                        logging.warning("Surface with small area and less than 10 faces found, ignoring it")
                        logging.warning(surface)
                    else:
                        surfaces.append(surface)
                else:        
                    surfaces.append(surface)
                    
                checked = checked + surface    
        
        return surfaces

class SurfaceForm(Enum):
    Flat = 0
    Cylindric = 1
    Spherical = 2
    
class StlSubmesh:
    
    def __init__(self, mesh, surfaceIds, i):
        self.mesh = trimesh.util.submesh(mesh, [surfaceIds])[0].copy()
        self.surfaceIds = surfaceIds
        self.id = i
        
        symAxis, symAxisDiff = self.get_symmetry_axis(100)
        if(symAxisDiff < 1e9):
            rot = su.get_rotation_from_vectors(symAxis,np.array([0,0,1]))
            rotrev = su.get_rotation_from_vectors(np.array([0,0,1]),symAxis)
            su.rotate_mesh(self.mesh,rot)
            symPoint, symPointDiff = self.get_symmetry_point(100)
            su.rotate_mesh(self.mesh,rotrev)
            
        planAxis, planAxisDiff = self.get_planarity_axis()
        
        if (symAxisDiff  < planAxisDiff):
            self.surfaceForm = SurfaceForm.Cylindric
            self.symAxis = symAxis
            self.symPoint = symPoint
            self.radius = self.get_cylinder_radius()
            
            self.rotProj = su.get_rotation_from_vectors(symAxis,np.array([0,0,1]))
            self.revRotProj = su.get_rotation_from_vectors(np.array([0,0,1]),symAxis)
            self.isTransformed = False
            logging.info("Submesh {0} has approximetly a cylindric shape".format(i))
        
        else:
            self.planAxis = planAxis
            self.surfaceForm = SurfaceForm.Flat
            
            self.rotProj = su.get_rotation_from_vectors(planAxis,np.array([0,0,1]))
            self.revRotProj = su.get_rotation_from_vectors(np.array([0,0,1]),planAxis)
            self.isTransformed = False
            logging.info("Submesh {0} has approximetly a planar shape".format(i))
        
        # properties of the projected surface
        self.transform_to_projection()
        self.p_points = self.get_plane_proj()
        self.transform_to_original()
        self.p_hull = ConvexHull(self.p_points)
        
        self.p_xmin = self.p_points[:,0].min()
        self.p_ymin = self.p_points[:,1].min()
        self.p_xmax = self.p_points[:,0].max()
        self.p_ymax = self.p_points[:,1].max()
        
        self.intPoints = np.array([])
        self.intNormals = np.array([])
        
    # estimation of best projection
    
    def get_symmetry_axis(self, maxIter):
        curEstimate = np.array([0,0,0])
        count = 0
        totalDiff = 0
        
        for i in range(maxIter):
            i1 = np.random.randint(0,len(self.mesh.face_normals))
            i2 = np.random.randint(0,len(self.mesh.face_normals))
            n1, n2 = -self.mesh.face_normals[i1], -self.mesh.face_normals[i2]
            axis = np.cross(n1,n2)
            if axis.dot(curEstimate) < 0:
                axis*=-1
            norm = np.sqrt(axis.dot(axis))
            if(norm > 1e-5):
                count += 1
                curEstimate = curEstimate + axis/norm
                diff = axis/norm-curEstimate/count
                totalDiff += np.sqrt(diff.dot(diff))
                
        if(count==0):
            return (curEstimate, 1e10)   
        
        curEstimate = curEstimate/count
        curEstimate = curEstimate/np.sqrt(curEstimate.dot(curEstimate))
        if(curEstimate[2] < 0):
            curEstimate *= -1
        return (curEstimate, totalDiff/count)
    
    def get_symmetry_point(self, maxIter):
        curEstimate = np.array([0,0])
        count = 0
        totalDiff = 0
        
        for i in range(maxIter):
            i1 = np.random.randint(0,len(self.mesh.face_normals))
            i2 = np.random.randint(0,len(self.mesh.face_normals))
            n1, n2 = self.mesh.face_normals[i1], self.mesh.face_normals[i2]
            
            if(np.abs(n1.dot(n2)) > 0.99):
                continue
            
            c1 = su.get_face_centroid(self.mesh,i1)
            c2 = su.get_face_centroid(self.mesh,i2)
            
            l1 = [c1[:2]-1e5*n1[:2], c1[:2]+1e5*n1[:2]]
            l2 = [c2[:2]-1e5*n2[:2], c2[:2]+1e5*n2[:2]]
            
            intersect = su.line_intersection_2d(l1,l2)
            
            if(intersect):
                count += 1
                curEstimate = curEstimate + intersect
                diff = intersect-curEstimate/count
                totalDiff += np.sqrt(diff.dot(diff))
        res = np.array([curEstimate[0],curEstimate[1],0])
        return (res/count, totalDiff/count)
    
    def get_cylinder_radius(self):
        i1 = np.random.randint(0,len(self.mesh.face_normals))
        n1 = self.mesh.face_normals[i1]
        c1 = su.get_face_centroid(self.mesh,i1)
        diff = c1[:2]-self.symPoint[:2]
        return np.sqrt(np.sum(diff**2))
    
    def get_planarity_axis(self):
        mean = self.mesh.face_normals.mean(axis=0)
        mean = mean/np.sqrt(mean.dot(mean))
        totalDiff = 0
        for n in self.mesh.face_normals:
            totalDiff += np.sqrt(((n-mean)**2).sum())
        return (mean, totalDiff/len(self.mesh.face_normals))
    
    # projection
    def get_plane_proj(self):
        if(not self.isTransformed):
            logging.info("Mesh is not rotated to projection orientation, will rotate it")
            self.transform_to_projection()
            
        arcs = np.zeros((len(self.mesh.vertices),2))
        for i in range(len(arcs)):
            if(self.surfaceForm == SurfaceForm.Flat):
                arcs[i] = su.proj_flat(self.mesh.vertices[i])
            elif(self.surfaceForm == SurfaceForm.Cylindric):
                arcs[i] = su.proj_cylindric(self.mesh.vertices[i])
                
        return arcs
    
    def transform_to_projection(self):
        if(not self.isTransformed):
            su.rotate_mesh(self.mesh,self.rotProj)
            if(self.surfaceForm == SurfaceForm.Cylindric):
                su.translate_mesh(self.mesh,-self.symPoint)
            self.isTransformed = True
        else:
            logging.info("Mesh is already rotated to projection orientation")
    
    def transform_to_original(self):
        if(self.isTransformed):
            if(self.surfaceForm == SurfaceForm.Cylindric):
                su.translate_mesh(self.mesh,self.symPoint)
            su.rotate_mesh(self.mesh,self.revRotProj)
            self.isTransformed = False
        else:
            logging.info("Mesh is already rotated to original orientation")
        
        
    # Ray intersection calculations
            
    def set_proj_points_normals(self, points2d):
        if(not self.isTransformed):
            logging.info("Mesh is not rotated to projection orientation, will rotate it")
            self.transform_to_projection()
            
        points3d = np.ones((len(points2d),3))*1000
        normals3d = np.zeros((len(points2d),3))
        
        if (self.surfaceForm == SurfaceForm.Flat):
            get_ray = su.get_ray_flat
            plane_proj = su.proj_flat
            
        elif (self.surfaceForm == SurfaceForm.Cylindric):
            get_ray = su.get_ray_cylindric
            plane_proj = su.proj_cylindric
        
        self.mesh.unmerge_vertices()
        triangles = self.mesh.vertices.reshape((int(len(self.mesh.vertices)/3),3,3)).copy()
        normals = self.mesh.face_normals.copy()
        self.mesh.merge_vertices()
        
        for i in range(len(points2d)):
            for j in range(len(triangles)):
                projTriangles = np.array([plane_proj(p) for p in triangles[j]])
                if(su.point_in_triangle_2d(points2d[i],projTriangles, False)):
                    p1,p2,p3 = triangles[j]
                    q1,q2 = get_ray(points2d[i])
                    intersection = su.intersect_line_triangle(q1,q2,p1,p2,p3)
                    if(intersection is not None):
                        points3d[i] = intersection
                        normals3d[i] = normals[j]
                        break    
                        
        mask = (points3d[:] != [1000,1000,1000]).any(axis=1)
        self.intPoints = points3d[mask]
        self.intNormals = -normals3d[mask]
        
        center = self.symPoint if self.surfaceForm == SurfaceForm.Cylindric else 0
        for i in range(len(self.intPoints)):
            self.intPoints[i] = self.revRotProj.apply(self.intPoints[i]+center)
            self.intNormals[i] = self.revRotProj.apply(self.intNormals[i])
    