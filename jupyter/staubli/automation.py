import utils as su
from stlprocessing import *
import visualize as sv
import matplotlib.patches as mpatches
from project import StaubliProject
import time
from jsonschema import validate
import json
import logging
import os
import warnings
from argparse import ArgumentParser

def load_validate_configs(schemaPath="", jsonPath=""):
    with open(schemaPath + 'config.schema.json') as f:
        configSchema = json.load(f)
    with open(schemaPath + 'printing.schema.json') as f:
        printingSchema = json.load(f)
    
    with open(jsonPath + 'config.json') as f:
        config = json.load(f)
    with open(jsonPath + 'printing.json') as f:
        printing = json.load(f)
    
    validate(instance=config, schema=configSchema)
    validate(instance=printing, schema=printingSchema)
    
    return (config, printing)

def add_code(sp,name,x,y,z,rx,ry,rz,codePreDrip, codePostDrip,run):
    pos = len(sp.code)-1
    for line in codePreDrip:
        sp.addLine(pos, "  "+\
                   line.replace("$pName",name).replace("$run",str(run))\
                       .replace("$x", str(x)).replace("$y", str(y)).replace("$z", str(z))\
                       .replace("$rx", str(rx)).replace("$ry", str(ry)).replace("$rz", str(rz)))
        pos += 1
    sp.addLine(pos,"  movel({0},flange,mNomSpeed)".format(name))
    sp.addLine(pos+1, "  waitEndMove()")
    for line in codePostDrip:
        sp.addLine(pos, "  "+\
                   line.replace("$pName",name)\
                       .replace("$x", str(x)).replace("$y", str(y)).replace("$z", str(z))\
                       .replace("$rx", str(rx)).replace("$ry", str(ry)).replace("$rz", str(rz)))
        pos += 1

def main(schemaPath, jsonPath):
    config, printing = load_validate_configs(schemaPath=schemaPath, jsonPath=jsonPath)
    if not os.path.isdir(config["staubliFolder"]):
        os.mkdir(config["staubliFolder"])
        
    logging.basicConfig(filename=config["staubliFolder"]+'stlPrinting.log', level=logging.INFO,\
                       format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    logging.info('Start loading and validating jsons')
    
    # Processing stl file including subdivision into submeshes
    logging.info('Loading and processing STL file')
    valve = StlMesh(config["stlPath"])
    
    if(config["generateImages"]):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            logging.info('Generating image of full valve model')
            figure = plt.figure()
            #figure.set_size_inches(14, 11)
            axes = mplot3d.Axes3D(figure,auto_add_to_figure=False)
            figure.add_axes(axes)
            sv.matplot_valve(axes, valve)
            figure.savefig(config["staubliFolder"] + "valveFull.png")
            plt.close(figure)

        
    if(not config["directProcessing"]):
        input("Hit enter once the printing config file has been adjusted")
        config, printing = load_validate_configs(schemaPath=schemaPath, jsonPath=jsonPath)
        
    globalScale = valve.mesh.vertices.copy().flatten()
    submeshIdMap = np.zeros(len(valve.submeshes), dtype=np.int64)
    
    for i in range(len(valve.submeshes)):
        logging.info('Processing subleaf {0} of {1}'.format(i, len(valve.submeshes)))
        if(not printing["reusePrinting"]):
            submeshId = 0
            while(submeshId < len(printing["submeshPrinting"][submeshId])\
                  and printing["submeshPrinting"][submeshId]["submeshId"] != i):
                submeshId += 1
            
            if(printing["submeshPrinting"][submeshId]["submeshId"] != i):
                logging.info('Could not find subleaf id in printing json {0}'.format(i))
                
            submeshIdMap[i] = submeshId
        
        subleaf = valve.submeshes[i]
        
        if(config["generateImages"]):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                subleaf.transform_to_original()
                figure = plt.figure()
                figure.set_size_inches(14, 11)
                axes = mplot3d.Axes3D(figure,auto_add_to_figure=False)
                figure.add_axes(axes)
                sv.matplot_mesh(axes, subleaf, c=sv.get_color(i))
                scale = valve.submeshes[i].mesh.vertices
                
                axes.auto_scale_xyz(globalScale, globalScale, globalScale)
                patches = [mpatches.Patch(color=sv.get_color(i), label="submesh {0}".format(i))]
                axes.legend(handles=patches)
                
                plt.savefig(config["staubliFolder"] + "subleaf" + str(i) + "Mesh.png")
                plt.close(figure)
            
        # Generate Points on projection surface
        logging.info('Generate Points on projection surface')
        pointsConfig = printing["submeshPrinting"][submeshIdMap[i]]["points"]
        dy = pointsConfig["distance"]
        
        if subleaf.surfaceForm == SurfaceForm.Flat:
            dx = dy
        else:
            dx = dy/subleaf.radius
            
        if(pointsConfig["pattern"] == "topDown"):
            grid = su.topDownGrid(subleaf.p_xmin-dx*0.5, subleaf.p_ymin-dy,\
                             subleaf.p_xmax+dx*0.5, subleaf.p_ymax+dy, dx,dy)
        else: 
            grid = su.topDownGrid(subleaf.p_xmin-dx, subleaf.p_ymin-dy,\
                              subleaf.p_xmax+dx, subleaf.p_ymax+dy, dx,dy).T
        
        mask = su.points_in_hull(grid, subleaf.p_hull)
        
        if(config["generateImages"]):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)            
                subleaf.transform_to_projection()
                figure = plt.figure()
                figure.set_size_inches(14, 11)
                axes = figure.add_subplot(1, 1, 1)
                sv.plot_points_hull(axes, subleaf, grid)
                
                plt.savefig(config["staubliFolder"] + "subleaf" + str(i) + "Map.png")
                plt.close(figure)
                subleaf.transform_to_original()
        
        # Ray intersection calculation to get point on mesh
        logging.info('Starting the ray intersection calculation')
        start = time.time()
        subleaf.transform_to_projection()
        subleaf.set_proj_points_normals(grid[mask])
        end = time.time()
        logging.info('Finished ray intersection after {0}s'.format(round(end-start,3)))
        
    if(config["generateImages"]):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            figure = plt.figure()
            figure.set_size_inches(14, 11)
            axes = mplot3d.Axes3D(figure,auto_add_to_figure=False)
            figure.add_axes(axes)
            
            
            patches = []
            for i in range(len(valve.submeshes)):
                subleaf = valve.submeshes[i]
                subleaf.transform_to_original()
                sv.matplot_mesh(axes, subleaf, True, False, sv.get_color(i))
                patches.append(mpatches.Patch(color=sv.get_color(i), label="submesh {0}".format(i)))
            
            axes.auto_scale_xyz(globalScale, globalScale, globalScale)
            axes.legend(handles=patches)
            plt.savefig(config["staubliFolder"] + "ValvePointsFull.png")
            plt.close(figure)
            
            subleaf.transform_to_original()
            
    # Generate the staubli project code
    if(config["generateStaubliFile"]):
        logging.info('Generating staubli project code')
        for i in range(len(valve.submeshes)):
            if(config["singleStaulbiFile"]):
                sp = StaubliProject(config["staubliFolder"] + \
                                    config["staubliFolder"].split("/")[-2]+"/", new=(i==0))
            else:
                sp = StaubliProject(config["staubliFolder"] + "/subleaf" + str(i)+ "/", new=True)
            sp.addLine(len(sp.code)-1,"  movej(jStart,flange,mNomSpeed)")
            subleaf = valve.submeshes[i]
            runs = int(printing["submeshPrinting"][submeshIdMap[i]]["thickness"]/\
                       printing["submeshPrinting"][submeshIdMap[i]]["thicknessPerRun"])

            with warnings.catch_warnings():
                warnings.simplefilter(action='ignore', category=FutureWarning)
                for run in range(runs):
                    for j in range(len(subleaf.intPoints)):
                        
                        if(subleaf.intNormals[j].dot([0,0,1]) > 0.99):
                            x,y,z = subleaf.intPoints[j]
                            rx,ry,rz = 0,0,0
                        else:
                            if(subleaf.surfaceForm == SurfaceForm.Cylindric):
                                leafZCorrected = subleaf.symAxis if subleaf.symAxis[2] > -0.01 else -subleaf.symAxis
                            else:
                                if(np.abs(subleaf.planAxis.dot([0,0,1])) > 0.99):
                                    leafZCorrected = np.cross(subleaf.planAxis, np.array([0,1,0]))
                                else:
                                    leafZCorrected = np.cross(subleaf.planAxis, np.array([0,0,1]))
                            r1 = su.get_rotation_from_vectors(leafZCorrected,np.array([0,1,0]))
                            r2 = su.get_rotation_from_vectors(r1.apply(subleaf.intNormals[j]),np.array([0,0,1]))
                            if(subleaf.intNormals[j][2] < -0.01):
                                r2 = su.get_rotation_from_vectors(r1.apply(-subleaf.intNormals[j]),np.array([0,0,1]))
                            
                            if((r2*r1).apply(np.array([0,0,1]))[2] < -0.01):
                                print(i, (r2*r1).apply(np.array([0,0,1])))
                            x,y,z = -(r2*r1).apply(subleaf.intPoints[j])
                            rx,ry,rz = (r2*r1).as_euler('XYZ', degrees=True)
                            
                        
                        if (run == 0):
                            name = su.id_to_string(len(sp.points))
                            sp.addPoint(name,x,y,z,rx,ry,rz)
                        else:
                            name = su.id_to_string(len(sp.points)-len(subleaf.intPoints)+j)
                            
                        add_code(sp,name,x,y,z-printing["submeshPrinting"][submeshIdMap[i]]["dripHeight"],rx,ry,rz, 
                                       printing["submeshPrinting"][submeshIdMap[i]]["codePreDrip"],\
                                       printing["submeshPrinting"][submeshIdMap[i]]["codePostDrip"],run)
            sp.save()
    return valve
    
    
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-s", "--schemaPath", dest="schemaPath",
                        help="path of the json schema files", default="staubli/")
    parser.add_argument("-j", "--jsonPath", dest="jsonPath",
                        help="path of the json files", default="")
    
    args = parser.parse_args()
    main(args.schemaPath, args.jsonPath)