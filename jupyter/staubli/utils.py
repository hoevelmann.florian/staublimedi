import numpy as np
from scipy.spatial.transform import Rotation as R
import logging
import warnings

def get_rotation_from_vectors(vec1,vec2):
    vec1 = vec1/np.sqrt(np.sum(vec1**2))
    vec2 = vec2/np.sqrt(np.sum(vec2**2))
    angle = np.arccos(np.dot(vec1, vec2))
    if (angle < 0.01 or angle > 3.14):
        logging.info("Angle between vectors describing rotation is small, using the identity")
        return R.identity()
    
    rotVec = np.cross(vec1, vec2)
    rotVec = rotVec/np.sqrt(np.sum(rotVec**2))
    return R.from_rotvec(angle*rotVec)

def check_normals_inwards(mesh):
    mesh.unmerge_vertices()
    inwards = mesh.face_normals[0].dot(mesh.vertices[0]) < 0.001
        
    mesh.merge_vertices()
    
    if(not inwards):
        mesh.invert()
        
# intersection functions

def line_intersection_2d(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return False
    
    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return [x, y]

pi2 = 2*np.pi-1e-6
def point_in_triangle_2d(p,vertices,flipTriangle):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        vertices = vertices -p
        n1,n2,n3 = np.sqrt(np.sum(vertices**2,axis=1))
        if(flipTriangle):
            v3,v2,v1 = vertices[0]/n1, vertices[1]/n2, vertices[2]/n3 
        else:
            v1,v2,v3 = vertices[0]/n1, vertices[1]/n2, vertices[2]/n3 
        angles = np.arccos(np.dot(v1,v2))+np.arccos(np.dot(v2,v3))+np.arccos(np.dot(v3,v1))
        return angles > pi2 or np.isnan(angles)
    
def intersect_line_triangle(q1,q2,p1,p2,p3):
    def signed_tetra_volume(a,b,c,d):
        return np.sign(np.dot(np.cross(b-a,c-a),d-a)/6.0)

    s1 = signed_tetra_volume(q1,p1,p2,p3)
    s2 = signed_tetra_volume(q2,p1,p2,p3)

    if s1 != s2:
        s3 = signed_tetra_volume(q1,q2,p1,p2)
        s4 = signed_tetra_volume(q1,q2,p2,p3)
        s5 = signed_tetra_volume(q1,q2,p3,p1)
        if s3 == s4 and s4 == s5:
            n = np.cross(p2-p1,p3-p1)
            t = np.dot(p1-q1,n) / np.dot(q2-q1,n)
            return q1 + t * (q2-q1)
    return None


def get_ray_flat(p):
    return np.array([[p[0],p[1],-100000],\
                     [p[0],p[1],100000]])

def get_ray_cylindric(p):
    q1 = [np.cos(-p[0])*1000000,np.sin(-p[0])*1000000,p[1]]
    q2 = [0,0,p[1]]
    
    return np.array([q1,q2])

def get_face_centroid(mesh, i):
    centroid1 = np.array([0,0,0])
    for j in mesh.faces[i]:
        centroid1 = centroid1 + mesh.vertices[j]
        
    return centroid1/3

# Defining grids on the projection plane

def topDownGrid(xmin,ymin,xmax,ymax,dx,dy):
    xdim = int(np.ceil((xmax-xmin)/dx))+1
    ydim = int(np.ceil((ymax-ymin)/dy))+1

    xscale = (xmax-xmin)/(xdim-1)
    yscale = (ymax-ymin)/(ydim-1)
    

    grid = np.zeros((xdim*ydim,2))
    for i in range(xdim):
        for j in range(ydim):
            if(i%2 == 0):
                grid[i*ydim+j] = np.array([i*xscale+xmin,j*yscale+ymin])
            else:
                grid[i*ydim+j] = np.array([i*xscale+xmin,(ydim-j-1)*yscale+ymin])
    return grid

def inwardGrid(xmin,ymin,xmax,ymax,dx,dy):
    xdim = int((xmax-xmin)/dx)+1
    ydim = int((ymax-ymin)/dy)+1
    
    grid = np.zeros((xdim*ydim,2))
    cur = 1
    grid[0] = np.array([xmin,ymin])
    mode = 0 #0:top, 1:right, 2:down, 3:left
    while(xdim > 0 and ydim > 0):
        steps = 0
        if(mode%2==0):
            while(steps < xdim-1):
                shift = np.array([0,dy]) if mode==0 else -np.array([0,dy])
                grid[cur] = grid[cur-1]+shift
                steps += 1
                cur += 1
            xdim -= 1 if cur > xdim else 0
        else:
            while(steps < ydim-1):
                shift = np.array([dx,0]) if mode==1 else -np.array([dx,0])
                grid[cur] = grid[cur-1]+shift
                steps += 1
                cur += 1
            ydim -= 1
        mode = (mode+1)%4
    return grid

def point_in_hull(point, hull, tolerance=1e-12):
    return all(
        (np.dot(eq[:-1], point) + eq[-1] <= tolerance)
        for eq in hull.equations)

def points_in_hull(points, hull, tolerance=1e-12):
    mask = np.zeros(len(points),dtype=bool)
    for i in range(len(points)):
        if(point_in_hull(points[i],hull)):
            mask[i] = True
    return mask

def proj_cylindric(p):
    vlen = np.sqrt(p[:2].dot(p[:2]))
    arc = np.array([np.arccos(np.dot([1,0], p[:2])/vlen),p[2]])
    if p[1] > 0:
        arc[0] = 2*np.pi - arc[0]
    return arc

def proj_flat(p):
    return p[:2]


# rigid transformations

def rotate_mesh(mesh,r):

    for i in range(len(mesh.vertices)):
        mesh.vertices[i] = r.apply(mesh.vertices[i])
    
    mesh.fix_normals()
    check_normals_inwards(mesh)
    
    
def translate_mesh(mesh,v):
    for i in range(len(mesh.vertices)):
        mesh.vertices[i] = mesh.vertices[i]+v
        

# functions required creating staubli code

def get_euler_angles(vec1, vec2=np.array([0,0,1])):
    vec1 = vec1/np.sqrt(np.sum(vec1**2))
    vec2 = vec2/np.sqrt(np.sum(vec2**2))
    angle = np.arccos(np.dot(vec1, vec2))
    if (angle < 0.01):
        return 0,0,0
    rotVec = np.cross(vec2, vec1)
    rotVec = rotVec/np.sqrt(np.sum(rotVec**2))
    r = R.from_rotvec(angle*rotVec)
    a,b,c = r.as_euler('xyz', degrees=True)
    #r2 = R.from_euler('xyz', [a,b,c], degrees=True)
    return a,b,c

def id_to_string(num):
    cpy = num
    res = ""
    while(cpy/25 > 1):
        res += str(chr(97+(cpy%26)))
        cpy = cpy//26-1
    res += str(chr(97+(cpy%26)))
    if(res[::-1] == "if" or res[::-1] == "do"):
        res = "xxxxxxxxxx" + res
    return res[::-1]
