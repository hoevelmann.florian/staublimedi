import xml.etree.ElementTree as ET
import pandas as pd
import numpy as np
from functools import reduce
from shutil import copyfile
import os
import logging

class StaubliProject:
    def __createNew(self, templatePath):
        try:
            if not os.path.isdir(self.folder):
                os.mkdir(self.folder)
            copyfile(templatePath + "start.pgx", self.folder + "start.pgx")
            copyfile(templatePath + "stop.pgx", self.folder + "stop.pgx")
            copyfile(templatePath + "template.dtx", self.folder + self.name + ".dtx")
            copyfile(templatePath + "template.pjx", self.folder + self.name + ".pjx")
        except OSError:
            logging.error("Creation of the directory %s failed" % self.folder)
        
    def __prepareProjectFile(self):
        pTree = ET.parse(self.folder + self.name + ".pjx")
        pRoot = pTree.getroot()
        
        pRoot.find("{ProjectNameSpace}dataSection")\
             .find("{ProjectNameSpace}data")\
             .set("file", self.name + ".dtx")
        self.__register_all_namespaces(self.folder + self.name + ".pjx")
        pTree.write(self.folder + self.name + ".pjx",\
                   encoding='utf-8', xml_declaration=True)
        
    def __addPointT(self,name,x,y,z,rx,ry,rz):
        elemS = """
        <point name="{0}" public="false">
          <pFather alias="" name="fWork" fatherIndex="0" />
          <valuePoint index="0">
            <tpValue x="{1}" y="{2}" z="{3}" rx="{4}" ry="{5}" rz="{6}" />
            <cpValue shoulder="ssame" elbow="esame" wrist="wsame" />
          </valuePoint>
        </point>""".format(name,x,y,z,rx,ry,rz)
        elem = ET.fromstring(elemS)
        self.__dataRoot.find('{DataNameSpace}pointSection').append(elem)
    
    def __removePointT(self,name):
        points = self.__dataRoot.find('{DataNameSpace}pointSection')\
                                .findall('{DataNameSpace}point')
        for point in points:
            if(point.get("name") == name):
                self.__dataRoot.find('{DataNameSpace}pointSection').remove(point)
                
    def __clearPointsT(self):
        points = self.__dataRoot.find('{DataNameSpace}pointSection')\
                                .findall('{DataNameSpace}point')
        for point in points:
            self.__dataRoot.find('{DataNameSpace}pointSection').remove(point)
    
    def __register_all_namespaces(self, filename):
        namespaces = dict([node for _, node in ET.iterparse(filename, events=['start-ns'])])
        for ns in namespaces:
            ET.register_namespace(ns, namespaces[ns])
        
    def __loadPoints(self):
        points = self.__dataRoot.find('{DataNameSpace}pointSection')\
                                .findall('{DataNameSpace}point')
        data = np.array([[]])
        
        for point in points:
            name = point.get('name')
            val = point[1].find('{DataNameSpace}tpValue')
            x,y,z = val.get('x'), val.get('y'), val.get('z')
            rx,ry,rz = val.get('rx'), val.get('ry'), val.get('rz')
            data = np.append(data, np.array([name,x,y,z,rx,ry,rz]))
        
        data = data.reshape((len(points),7))
        df = pd.DataFrame(data,columns=["name", "x", "y", "z", "rx", "ry", "rz"])
        return df
    
    def __loadCode(self):
        codeElem = self.__codeRoot[0].find("{ProgramNameSpace}source")[0]
        return np.array(codeElem.text.split("\n")).astype('U50') 
    
    def __init__(self, folder, new=False):
        self.folder = folder
        self.name = folder.split("/")[-2]
        
        if(new):
            self.__createNew("staubli/template/")
            
        self.__prepareProjectFile()
        
        # Attributes for data
        self.__dataTree = ET.parse(folder+self.name+".dtx")
        self.__dataRoot = self.__dataTree.getroot()
        self.points = self.__loadPoints()
        
        # Attributes for code
        self.__codeTree = ET.parse(folder+"start.pgx")
        self.__codeRoot = self.__codeTree.getroot()
        self.code = self.__loadCode()
        
    def saveCode(self):
        text = reduce(lambda a,b :a+"\n"+b,self.code)
        self.__codeRoot[0].find("{ProgramNameSpace}source")[0].text = text
        path = self.folder + "start.pgx"
        self.__register_all_namespaces(path)
        self.__codeTree.write(path,encoding='utf-8', xml_declaration=True)
    
    def addLine(self,pos,line):
        self.code = np.insert(self.code, pos, line)
        
    def removeLine(self,pos):
        self.code = np.delete(self.code, pos)
    
    def savePoints(self):
        self.__clearPointsT()
        for index, row in self.points.iterrows():
            self.__addPointT(row["name"], row.x, row.y, row.z,\
                            row.rx, row.ry, row.rz)
        self.__register_all_namespaces(self.folder+self.name+".dtx")
        self.__dataTree.write(self.folder + self.name + ".dtx",\
                   encoding='utf-8', xml_declaration=True)
    
    def addPoint(self,name,x,y,z,rx,ry,rz):
        if(len(self.points[self.points["name"] == name])>0):
            logging.info("Point {0} does already exists".format(name))
            return
        
        row = {"name": name, "x": x, "y": y, "z": z, \
               "rx": rx, "ry": ry, "rz": rz}
        self.points = self.points.append(row, ignore_index=True)
        
    def removePoint(self,name):   
        self.points = self.points.drop(self.points.index[self.points["name"] == name])
    
    def clearProject(self):
        self.__clearPointsT()
        self.points = self.points.iloc[0:0]
        self.code = np.array(["begin", "end"]).astype('U50')
    
    def save(self):
        self.savePoints()
        self.saveCode()
