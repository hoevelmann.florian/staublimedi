#!/bin/bash
sudo docker start staublicontainer 

if [ -n "$1" ]; then 
  if [ -n "$2" ]; then
    sudo docker exec -it staublicontainer python staubli/automation.py -j $1 -s $2
  else
    echo "Schema path is not set, using default"
    sudo docker exec -it staublicontainer python staubli/automation.py -j $1
  fi
else
  echo "Json path is required, please try again"
fi

sudo docker stop staublicontainer
