#!/bin/bash
sudo docker build -t staubliimage .

sudo docker run -p 8888:8888 -d -v "${PWD}/jupyter":/home/jovyan --name staublicontainer staubliimage

sudo docker stop staublicontainer

