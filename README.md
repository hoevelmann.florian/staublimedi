# StaubliMedi

## Requirements
Docker

## Install
`./install.sh`

## Run automation script
The working directory of the docker image is the folder jupyter. First argument defines the folder containing the JSON files and the second where the JSON schemas are stored. By default the schemas are stored in jupyter/staubli/.

`sudo ./automation.sh valveTest/`

## Start Jupyter server for interactive editing
`sudo docker start staublicontainer`

The command below will start the docker container which runs the jupyter server. In the browser we can access this server under:
http://127.0.0.1:8888/tree

## Set up a project
Information on how to properly set up a project including stl, config, printing files can be found in the Report.


### Manually setting up the docker image
```
sudo docker build -t staubliimage .

sudo docker run -p 8888:8888 -v "${PWD}/jupyter":/home/jovyan --name staublicontainer staubliimage

sudo docker stop staublicontainer
```


## Manually running the automation script
```
sudo docker exec -it staublicontainer bash
python staubli/automation.py -j valveTest/
```




